import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MarksComponent } from './marks/marks.component';
import { StudentDetailsComponent } from './students/student-details/student-details.component';
import { StudentsComponent } from './students/students.component';

const routes: Routes = [{path: "home", component: HomeComponent},
                        {path: "students", component: StudentsComponent},
                        {path: "students/:id", component: StudentDetailsComponent},
                        {path: "marks", component: MarksComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
