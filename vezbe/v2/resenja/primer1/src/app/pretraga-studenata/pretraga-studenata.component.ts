import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pretraga-studenata',
  templateUrl: './pretraga-studenata.component.html',
  styleUrls: ['./pretraga-studenata.component.css']
})
export class PretragaStudenataComponent implements OnInit {

  @Input()
  smerovi: any[] = [];

  @Output()
  pretraga: EventEmitter<any> = new EventEmitter<any>();

  parametri: any = {
    "brojIndeksa": undefined,
    ime: undefined,
    prezime: undefined,
    godinaUpisaOd: undefined,
    godinaUpisaDo: undefined,
    smer: undefined,
    "prosecnaOcenaOd": undefined,
    "prosecnaOcenaDo": undefined
  }

  constructor() { }

  ngOnInit(): void {
  }

  smerTrackBy(indeks: number, smer: any) {
    return smer.sifraSmera;
  }

  pretrazi() {
    this.pretraga.emit({ ...this.parametri });
  }
}
