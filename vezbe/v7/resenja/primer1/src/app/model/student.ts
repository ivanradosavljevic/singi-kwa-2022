import { Smer } from "./smer";

export interface Student {
    id: number;
    brojIndeksa: string;
    ime: string;
    prezime: string;
    smer: Smer;
    godinaUpisa: number;
    prosecnaOcena?: number;
}