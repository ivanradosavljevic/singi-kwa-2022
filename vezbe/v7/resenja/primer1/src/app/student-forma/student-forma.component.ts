import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Smer } from '../model/smer';
import { Student } from '../model/student';
import { SmeroviService } from '../service/smerovi.service';

@Component({
  selector: 'app-student-forma',
  templateUrl: './student-forma.component.html',
  styleUrls: ['./student-forma.component.css']
})
export class StudentFormaComponent implements OnInit {

  smerovi: Smer[] = [];

  studentForma : FormGroup = new FormGroup({
    "id": new FormControl(),
    "brojIndeksa": new FormControl(),
    "ime": new FormControl(),
    "prezime": new FormControl(),
    "godinaUpisa": new FormControl(),
    "smer": new FormControl(),
    "prosecnaOcena": new FormControl(undefined, [Validators.min(5), Validators.max(10)]),
  });

  @Output()
  studentSubmit: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  set student(student: Student) {
    this.studentForma.patchValue(student);
  }

  constructor(private smeroviServis : SmeroviService) { }

  ngOnInit(): void {
    this.smeroviServis.dobaviSve().subscribe(smerovi => {
      this.smerovi = smerovi;
    });
  }

  naSubmit() {
    if(this.studentForma.valid) {
      this.studentSubmit.emit(this.studentForma.value);
    }
  }

  smerTrackBy(indeks: number, smer: any) {
    return smer.sifraSmera;
  }

  resetForme() {
    this.studentForma.reset();
  }

  comparator(v1: any, v2: any) {
    if(v1 && v2)
    {
      return v1["sifraSmera"] == v2["sifraSmera"]
    } else {
      return  v1 == v2
    }
  }
}
