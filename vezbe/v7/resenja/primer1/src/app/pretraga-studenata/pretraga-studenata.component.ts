import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Smer } from '../model/smer';
import { SmeroviService } from '../service/smerovi.service';

@Component({
  selector: 'app-pretraga-studenata',
  templateUrl: './pretraga-studenata.component.html',
  styleUrls: ['./pretraga-studenata.component.css']
})
export class PretragaStudenataComponent implements OnInit {

  smerovi: Smer[] = [];

  parametriForma : FormGroup = new FormGroup({
    brojIndeksa: new FormControl(),
    ime: new FormControl(),
    prezime: new FormControl(),
    godinaUpisaOd: new FormControl(),
    godinaUpisaDo: new FormControl(),
    smer: new FormControl(),
    prosecnaOcenaOd: new FormControl(),
    prosecnaOcenaDo: new FormControl()
  });

  @Output()
  pretraga: EventEmitter<any> = new EventEmitter<any>();

  constructor(private smeroviServis : SmeroviService) { }

  ngOnInit(): void {
    this.smeroviServis.dobaviSve().subscribe(smerovi => {
      this.smerovi = smerovi;
    });
  }

  smerTrackBy(indeks: number, smer: any) {
    return smer.sifraSmera;
  }

  pretrazi() {
    this.pretraga.emit(this.parametriForma.value);
  }
}
