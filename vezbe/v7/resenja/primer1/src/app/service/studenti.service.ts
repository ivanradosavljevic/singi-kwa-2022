import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { filter, map } from 'rxjs';
import { Student } from '../model/student';

@Injectable({
  providedIn: 'root'
})
export class StudentiService {
  constructor(private client: HttpClient) { }

  pretraziStudente(parametri: any = undefined) {
    if (parametri == undefined) {
      return this.client.get<Student[]>("http://localhost:3000/studenti");
    }
    return this.client.get<Student[]>("http://localhost:3000/studenti").pipe(
      map(studenti => {
        return studenti.filter(student => {
          let rezultat = true;
          if (student["brojIndeksa"] && parametri["brojIndeksa"]) {
            rezultat &&= student["brojIndeksa"] == parametri["brojIndeksa"];
          }
          if (student["prosecnaOcena"] && parametri["prosecnaOcenaOd"]) {
            rezultat &&= student["prosecnaOcena"] >= parametri["prosecnaOcenaOd"]
          }
          return rezultat;
        });
      })
    );
  }

  dodajStudenta(student: Student) {
    console.log(student);
    return this.client.post("http://localhost:3000/studenti", student);
  }

  azurirajStudenta(id: number, student : Student) {
    return this.client.put(`http://localhost:3000/studenti/${id}`, student);
  }

  ukloni(id: number) {
    return this.client.delete(`http://localhost:3000/studenti/${id}`);
  }
}
