import { Component } from '@angular/core';
import { Student } from './model/student';
import { StudentiService } from './service/studenti.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Tabela studenata';

  studentZaIzmenu: any = {};
  parametri : any = {};
  filtriraniStudenti: Student[] = [];

  constructor(private studentiServis : StudentiService) {
    studentiServis.pretraziStudente().subscribe((studenti : Student[]) => {
      this.filtriraniStudenti = studenti;
    });
  }

  postaviZaIzmenu(red: any) {
    this.studentZaIzmenu = { ...red.value };
  }

  obradaStudenta(student: Student) {
    if(student.id != undefined) {
      this.studentiServis.azurirajStudenta(student.id, student).subscribe(_ => {
        this.pretraziStudente(this.parametri);
      });
    } else {
      this.studentiServis.dodajStudenta(student).subscribe(_ => {
        this.pretraziStudente(this.parametri);
      });
    }
  }

  ukloni(ukloniDogadjaj: any) {
    this.studentiServis.ukloni(ukloniDogadjaj.value.id).subscribe(_ => {
      this.pretraziStudente(this.parametri);
    });
  }

  pretraziStudente(parametri : any) {
    if(parametri === undefined) {
      parametri = this.parametri;
    } else {
      this.parametri = parametri;
    }
    this.studentiServis.pretraziStudente(parametri).subscribe((studenti : Student[]) => {
      this.filtriraniStudenti = studenti;
    });
  }
}
