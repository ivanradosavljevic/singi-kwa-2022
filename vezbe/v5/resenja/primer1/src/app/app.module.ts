import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabelaStudenataComponent } from './tabela-studenata/tabela-studenata.component';
import { PretragaStudenataComponent } from './pretraga-studenata/pretraga-studenata.component';
import { StudentFormaComponent } from './student-forma/student-forma.component';
import { ProveraNepostojecihVrednostiDirective } from './provera-nepostojecih-vrednosti.directive';
import { ObelezivacRedovaDirective } from './obelezivac-redova.directive';
import { PorukaOGresciDirective } from './poruka-ogresci.directive';

@NgModule({
  declarations: [
    AppComponent,
    TabelaStudenataComponent,
    PretragaStudenataComponent,
    StudentFormaComponent,
    ProveraNepostojecihVrednostiDirective,
    ObelezivacRedovaDirective,
    PorukaOGresciDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
