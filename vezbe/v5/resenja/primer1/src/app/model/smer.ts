export interface Smer {
    id: number;
    sifraSmera: string;
    naziv: string;
}
