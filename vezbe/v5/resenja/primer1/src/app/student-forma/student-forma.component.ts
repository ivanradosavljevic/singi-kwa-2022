import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Smer } from '../model/smer';
import { SmeroviService } from '../service/smerovi.service';

@Component({
  selector: 'app-student-forma',
  templateUrl: './student-forma.component.html',
  styleUrls: ['./student-forma.component.css']
})
export class StudentFormaComponent implements OnInit {

  smerovi: Smer[] = [];

  @Output()
  studentSubmit: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  student: any = {
    "originalniIndeks": undefined,
    "brojIndeksa": undefined,
    ime: undefined,
    prezime: undefined,
    godinaUpisa: undefined,
    smer:  undefined,
    "prosecnaOcena": undefined
  }

  constructor(private smeroviServis : SmeroviService) { }

  ngOnInit(): void {
    this.smeroviServis.dobaviSve().subscribe(smerovi => {
      this.smerovi = smerovi;
    });
  }

  naSubmit() {
    this.studentSubmit.emit({ ...this.student });
  }

  smerTrackBy(indeks: number, smer: any) {
    return smer.sifraSmera;
  }

  resetForme() {
    this.student = {};
  }

  comparator(v1: any, v2: any) {
    if(v1 && v2)
    {
      return v1["sifraSmera"] == v2["sifraSmera"]
    } else {
      return  v1 == v2
    }
  }
}
