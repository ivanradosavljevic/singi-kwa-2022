import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Smer } from '../model/smer';

@Injectable({
  providedIn: 'root'
})
export class SmeroviService {
  constructor(private client : HttpClient) { }

  dobaviSve() {
    return this.client.get<Smer[]>("http://localhost:3000/smerovi");
  }
}
