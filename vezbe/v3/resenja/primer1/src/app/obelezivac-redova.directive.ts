import { Directive, ElementRef, Host, HostListener } from '@angular/core';
import { elementAt } from 'rxjs';

@Directive({
  selector: '[appObelezivacRedova]'
})
export class ObelezivacRedovaDirective {
  private originalBackgroundColor: any = "";

  constructor(private element: ElementRef) { }

  @HostListener("mouseenter")
  public onMouseEnter() {
    this.originalBackgroundColor = this.element.nativeElement.style.backgroundColor
    this.element.nativeElement.style.backgroundColor = "lightgray";
  }

  @HostListener("mouseleave")
  public onMouseLeave() {
    this.element.nativeElement.style.backgroundColor = this.originalBackgroundColor;
  }

}
