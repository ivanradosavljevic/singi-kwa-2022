import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Tabela studenata';

  smerovi: any[] = [
    { "sifraSmera": "SII", "naziv": "Softversko i informaciono inzenjerstvo" },
    { "sifraSmera": "IT", "naziv": "Informacione tehnologije" },
  ]

  studenti: any[] = [
    {
      "brojIndeksa": "1234/1234567",
      "ime": "Petar",
      "prezime": "Petrović",
      "smer": { "sifraSmera": "SII", "naziv": "Softversko i informaciono inzenjerstvo" },
    },
    {
      "brojIndeksa": "1235/1234567",
      "ime": "Marko",
      "prezime": "Petrović",
      "smer": { "sifraSmera": "SII", "naziv": "Softversko i informaciono inzenjerstvo" },
      "godinaUpisa": "test",
      "prosecnaOcena": 5,
    },
    {
      "brojIndeksa": "1236/1234567",
      "ime": "Jovana",
      "prezime": "Marković",
      "smer": { "sifraSmera": "SII", "naziv": "Softversko i informaciono inzenjerstvo" },
      prosecnaOcena: 10,
    },
    {
      "brojIndeksa": "1237/1234567",
      "ime": "Petar",
      "prezime": "Petrović",
      "smer": { "sifraSmera": "IT", "naziv": "Informacione tehnologije" },
      prosecnaOcena: 7
    },
  ]

  studentZaIzmenu: any = {};
  parametri : any = {};
  filtriraniStudenti: any[] = [];

  constructor() {
    this.filtriraniStudenti = [...this.studenti];
  }

  postaviZaIzmenu(red: any) {
    this.studentZaIzmenu = { ...red.value, originalniIndeks: red.value["brojIndeksa"] };
  }

  obradaStudenta(student: any) {
    if (student["originalniIndeks"] != undefined) {
      let novi = { ...student };
      delete novi["originalniIndeks"];
      this.studenti[this.studenti.findIndex(s => s["brojIndeksa"] == student["originalniIndeks"])] = novi;
      this.pretraziStudente();
      return;
    }
    this.studenti.push({ ...student });
    this.pretraziStudente();
    return;
  }

  ukloni(ukloniDogadjaj: any) {
    this.studenti.splice(this.studenti.findIndex((student) => {
      if (student["brojIndeksa"] == ukloniDogadjaj["value"]["brojIndeksa"]) {
        return true;
      }
      return false;
    }), 1);
    this.pretraziStudente();
  }


  sort(kolona: string) {
    this.studenti.sort((a: any, b: any) => {
      if (a[kolona] == undefined) {
        return -1;
      }
      else if (a[kolona] < b[kolona]) {
        return -1;
      } else if (a[kolona] > b[kolona]) {
        return 1;
      }
      return 0;
    });
    this.pretraziStudente();
  }

  pretraziStudente(parametri: any = undefined) {
    if(parametri === undefined) {
      parametri = this.parametri;
    } else {
      this.parametri = parametri;
    }
    this.filtriraniStudenti = this.studenti.filter(student => {
      let rezultat = true;
      if (student["brojIndeksa"] && parametri["brojIndeksa"]) {
        rezultat &&= student["brojIndeksa"] == parametri["brojIndeksa"];
      }
      if (student["prosecnaOcena"] && parametri["prosecnaOcenaOd"]) {
        rezultat &&= student["prosecnaOcena"] >= parametri["prosecnaOcenaOd"]
      }
      return rezultat;
    });
  }
}
