export interface TableMetadata {
    columns: TableColumn[],
    allowedActions: GenericAction[]
}

interface TableColumn {
    key: string,
    title: string,
    footer?: FooterAgg,
    displayFn?: DisplayFn
}

interface FooterAgg {
    (data: any, key: string): any
}

interface DisplayFn {
    (cellValue: any, data: any, key: any): any
}

interface GenericAction {
    event: string,
    title: string,
    icon: string
}