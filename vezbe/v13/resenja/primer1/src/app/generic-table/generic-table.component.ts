import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TableMetadata } from './table-metadata';

// import { get } from 'lodash';
import 'lodash';
import * as _ from 'lodash';

@Component({
  selector: 'app-generic-table',
  templateUrl: './generic-table.component.html',
  styleUrls: ['./generic-table.component.css']
})
export class GenericTableComponent implements OnInit {
  @Input()
  data: any;

  @Input()
  metadata: TableMetadata | undefined;

  @Output()
  actionTriggered: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  get(row:any, key: string) {
    return _.get(row, key);
  }

  emitAction(event: any, data: any) {
    this.actionTriggered.emit({
      event,
      data
    })
  }

}
