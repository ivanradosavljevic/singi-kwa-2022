import { Component } from '@angular/core';
import { TableMetadata } from './generic-table/table-metadata';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'primer';
  studentiMetadata : TableMetadata = {
    columns: [
      {"key": "id", title: "Id"},
      {"key": "ime", title: "Ime"},
      {"key": "prezime", title: "Prezime"},
      {"key": "adresa", title: "Adresa", displayFn: (x)=>{
        return  `${x['grad']}, ${x['ulica']} ${x['broj']}`;
      }},
      {"key": "adresa.grad", "title": "Grad"},
      {"key": "ocena", title: "Ocena", footer: (data, key)=>{
        return data.map((x:any)=>x[key]).reduce((acc:any, v:any)=>acc+v)/data.length;
      }},
    ],
    allowedActions: [
      {"event": "delete", "title": "Ukloni", "icon": "url"},
      {"event": "edit", "title": "Izmeni", "icon": "url"},
    ]
  };
  studenti = [
    {"id": "nesto1", "ime": "ime1", "prezime": "prezime1", adresa: {"grad": "Novi Sad", "ulica": "Ulica1", broj: "1"}, "ocena": 10},
    {"id": "nesto2", "ime": "ime2", "prezime": "prezime2", adresa: {"grad": "Beograd", "ulica": "Ulica2", broj: "2"}, "ocena": 7},
    {"id": "nesto3", "ime": "ime3", "prezime": "prezime3", adresa: {"grad": "Novi Sad", "ulica": "Ulica3", broj: "3"}, "ocena": 10},
    {"id": "nesto4", "ime": "ime4", "prezime": "prezime4", adresa: {"grad": "Novi Sad", "ulica": "Ulica1", broj: "31"}, "ocena": 6},
  ]

  obradaBrisanja(event: any) {
    if(event.event == "delete") {
      console.log(event);
    }
  }
}
