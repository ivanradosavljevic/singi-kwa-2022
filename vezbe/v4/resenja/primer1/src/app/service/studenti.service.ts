import { Injectable } from '@angular/core';
import { Student } from '../model/student';

@Injectable({
  providedIn: 'root'
})
export class StudentiService {
  private studenti: Student[] = [
    {
      "brojIndeksa": "1234/1234567",
      "ime": "Petar",
      "prezime": "Petrović",
      "smer": { "sifraSmera": "SII", "naziv": "Softversko i informaciono inzenjerstvo" },
      godinaUpisa: 2018,
      prosecnaOcena: undefined
    },
    {
      "brojIndeksa": "1235/1234567",
      "ime": "Marko",
      "prezime": "Petrović",
      "smer": { "sifraSmera": "SII", "naziv": "Softversko i informaciono inzenjerstvo" },
      "godinaUpisa": 2020,
      "prosecnaOcena": 5,
    },
    {
      "brojIndeksa": "1236/1234567",
      "ime": "Jovana",
      "prezime": "Marković",
      "smer": { "sifraSmera": "SII", "naziv": "Softversko i informaciono inzenjerstvo" },
      godinaUpisa: 2019,
      prosecnaOcena: 10,
    },
    {
      "brojIndeksa": "1237/1234567",
      "ime": "Petar",
      "prezime": "Petrović",
      "smer": { "sifraSmera": "IT", "naziv": "Informacione tehnologije" },
      godinaUpisa: 2020,
      prosecnaOcena: 7
    },
  ]

  constructor() { }

  azuriranje(student: any) {
    if (student["originalniIndeks"] != undefined) {
      let novi = { ...student };
      delete novi["originalniIndeks"];
      this.studenti[this.studenti.findIndex(s => s["brojIndeksa"] == student["originalniIndeks"])] = novi;
      return;
    }
    this.studenti.push({ ...student });
    return;
  }

  ukloni(ukloniDogadjaj: any) {
    this.studenti.splice(this.studenti.findIndex((student) => {
      if (student["brojIndeksa"] == ukloniDogadjaj["value"]["brojIndeksa"]) {
        return true;
      }
      return false;
    }), 1);
  }

  pretraziStudente(parametri: any = undefined) {
    if (parametri == undefined) {
      return [...this.studenti];
    }
    return this.studenti.filter(student => {
      let rezultat = true;
      if (student["brojIndeksa"] && parametri["brojIndeksa"]) {
        rezultat &&= student["brojIndeksa"] == parametri["brojIndeksa"];
      }
      if (student["prosecnaOcena"] && parametri["prosecnaOcenaOd"]) {
        rezultat &&= student["prosecnaOcena"] >= parametri["prosecnaOcenaOd"]
      }
      return rezultat;
    });
  }

  sort(kolona: string) {
    this.studenti.sort((a: any, b: any) => {
      if (a[kolona] == undefined) {
        return -1;
      }
      else if (a[kolona] < b[kolona]) {
        return -1;
      } else if (a[kolona] > b[kolona]) {
        return 1;
      }
      return 0;
    });
  }
}
