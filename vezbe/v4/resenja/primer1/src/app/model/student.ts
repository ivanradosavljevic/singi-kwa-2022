import { Smer } from "./smer";

export interface Student {
    brojIndeksa: string;
    ime: string;
    prezime: string;
    smer: Smer;
    godinaUpisa: number;
    prosecnaOcena?: number;
}