# Vežbe 4
*Cilj vežbi: Rad sa Angular servisima.*

## Zadaci
1. Prethodni primer proširiti uvođenjem modela podataka. Model podataka opisati interfejsima.
2. Napraviti servise za osnovne CRUD operacije nad definisanim modelom.
3. Svu komunikaciju između komponenti realizovati preko servisa.